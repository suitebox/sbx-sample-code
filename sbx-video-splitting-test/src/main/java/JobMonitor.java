import com.amazonaws.services.mediaconvert.AWSMediaConvert;
import com.amazonaws.services.mediaconvert.model.GetJobRequest;

public class JobMonitor {

	public String checkFinalStatus(String job, AWSMediaConvert emf) {

		String finalStatus;
		GetJobRequest getJobRequest = new GetJobRequest().withId(job);
		finalStatus = emf.getJob(getJobRequest).getJob().getStatus().toString();
		if (!finalStatus.equalsIgnoreCase("ERROR")) {
			while (!finalStatus.equalsIgnoreCase("COMPLETE")) {

				System.out.println("JOB " + job + " IN STATUS: " + finalStatus + ", WAITING 30 SECONDS");
				try {
					Thread.sleep(30000);
					finalStatus = emf.getJob(getJobRequest).getJob().getStatus().toString();
					if (finalStatus.equalsIgnoreCase("ERROR"))
						break;
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			// System.out.println(finalStatus);
		}
		return finalStatus;
	}
}