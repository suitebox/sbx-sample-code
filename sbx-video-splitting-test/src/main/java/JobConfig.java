import java.util.ArrayList;
import java.util.List;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.mediaconvert.*;
import com.amazonaws.services.mediaconvert.model.*;

public class JobConfig {
	private static final String role = "arn:aws:iam::766219056525:role/sbx-mediaconvert";
	private static final String region = "eu-central-1";
	private static final String endpoint = "https://usryickja.mediaconvert.eu-central-1.amazonaws.com";
	private String jobID;

	public String configure(String startTime, String endTime, String name, String queue, String fileInput,
			String fileOutput) {
		try {

			AWSMediaConvert emf = AWSMediaConvertClientBuilder.standard().withRegion(region).build();

			EndpointConfiguration endpointConfiguration = new EndpointConfiguration(endpoint, region);
			emf = AWSMediaConvertClientBuilder.standard().withEndpointConfiguration(endpointConfiguration).build();

			// Create job Request
			CreateJobRequest createJobRequest = new CreateJobRequest();

			// Assign role and queue to use in the service
			createJobRequest.withQueue(queue).withUserMetadata(null).withRole(role);

			// create job settings, probably better to do it in a separate class and to
			// invoke the methods here
			JobSettings settings = new JobSettings();

			OutputGroup output = new OutputGroup();
			output.setName("File Group");

			ContainerSettings cSettings = new ContainerSettings().withContainer("MP4");
			Mp4Settings mp4 = new Mp4Settings();

			mp4.setCslgAtom("INCLUDE");
			mp4.setCttsVersion(0);
			mp4.setFreeSpaceBox("EXCLUDE");
			mp4.setMoovPlacement("PROGRESSIVE_DOWNLOAD");

			cSettings.setMp4Settings(mp4);

			VideoDescription videoDesc = new VideoDescription();
			videoDesc.setScalingBehavior("DEFAULT");
			videoDesc.setTimecodeInsertion("DISABLED");
			videoDesc.setAntiAlias("ENABLED");
			videoDesc.setSharpness(50);

			VideoCodecSettings videoCodec = new VideoCodecSettings();

			videoCodec.setCodec("H_264");

			H264Settings h264 = new H264Settings();
			h264.setInterlaceMode("PROGRESSIVE");
			h264.setNumberReferenceFrames(3);
			h264.setSyntax("DEFAULT");
			h264.setSoftness(0);
			h264.setGopClosedCadence(1);
			h264.setGopSize(90d);
			h264.setSlices(1);
			h264.setGopBReference("DISABLED");
			h264.setSlowPal("DISABLED");
			h264.setSpatialAdaptiveQuantization("ENABLED");
			h264.setTemporalAdaptiveQuantization("ENABLED");
			h264.setFlickerAdaptiveQuantization("DISABLED");
			h264.setEntropyEncoding("CABAC");
			h264.setBitrate(1160000);
			h264.setFramerateControl("INITIALIZE_FROM_SOURCE");
			h264.setRateControlMode("CBR");
			h264.setCodecProfile("MAIN");
			h264.setTelecine("NONE");
			h264.setMinIInterval(0);
			h264.setAdaptiveQuantization("HIGH");
			h264.setCodecLevel("AUTO");
			h264.setFieldEncoding("PAFF");
			h264.setSceneChangeDetect("ENABLED");
			h264.setQualityTuningLevel("SINGLE_PASS");
			h264.setFramerateConversionAlgorithm("DUPLICATE_DROP");
			h264.setUnregisteredSeiTimecode("DISABLED");
			h264.setGopSizeUnits("FRAMES");
			h264.setParControl("INITIALIZE_FROM_SOURCE");
			h264.setNumberBFramesBetweenReferenceFrames(2);
			h264.setRepeatPps("DISABLED");
			h264.setDynamicSubGop("STATIC");

			videoCodec.setH264Settings(h264);

			videoDesc.setCodecSettings(videoCodec);
			videoDesc.setAfdSignaling("NONE");
			videoDesc.setDropFrameTimecode("ENABLED");
			videoDesc.setRespondToAfd("NONE");
			videoDesc.setColorMetadata("INSERT");

			AacSettings aac = new AacSettings();
			aac.setAudioDescriptionBroadcasterMix("NORMAL");
			aac.setBitrate(96000);
			aac.setRateControlMode("CBR");
			aac.setCodecProfile("LC");
			aac.setCodingMode("CODING_MODE_2_0");
			aac.setRawFormat("NONE");
			aac.setSampleRate(48000);
			aac.setSpecification("MPEG4");

			AudioCodecSettings audioCodec = new AudioCodecSettings();

			audioCodec.setCodec("AAC");
			audioCodec.setAacSettings(aac);

			AudioDescription audioDesc = new AudioDescription();
			audioDesc.setLanguageCodeControl("FOLLOW_INPUT");
			audioDesc.setCodecSettings(audioCodec);

			List<AudioDescription> listAudioDesc = new ArrayList<AudioDescription>();
			listAudioDesc.add(audioDesc);

			Output out = new Output();
			out.setContainerSettings(cSettings);
			out.setVideoDescription(videoDesc);
			out.setAudioDescriptions(listAudioDesc);
			out.setExtension("mp4");
			out.setNameModifier(name);

			List<Output> outputs = new ArrayList<Output>();
			outputs.add(out);

			OutputGroupSettings outputSettings = new OutputGroupSettings();
			outputSettings.setType("FILE_GROUP_SETTINGS");
			FileGroupSettings fileSettings = new FileGroupSettings();
			fileSettings.setDestination(fileOutput);
			outputSettings.setFileGroupSettings(fileSettings);

			output.setOutputs(outputs);
			output.setOutputGroupSettings(outputSettings);

			List<OutputGroup> outputGroups = new ArrayList<OutputGroup>();
			outputGroups.add(output);

			Input input = new Input();

			InputClipping clipping = new InputClipping();
			clipping.setEndTimecode(endTime);
			clipping.setStartTimecode(startTime);

			List<InputClipping> inputClippings = new ArrayList<InputClipping>();
			inputClippings.add(clipping);

			AudioSelector audioSelector = new AudioSelector();
			audioSelector.setOffset(0);
			audioSelector.setDefaultSelection("DEFAULT");
			audioSelector.setProgramSelection(1);

			VideoSelector videoSelector = new VideoSelector();

			videoSelector.setColorSpace("FOLLOW");
			videoSelector.setRotate("DEGREE_0");
			videoSelector.setAlphaBehavior("DISCARD");

			input.setInputClippings(inputClippings);
			input.addAudioSelectorsEntry("Audio Selector 1", audioSelector);
			input.setVideoSelector(videoSelector);
			input.setFilterEnable("AUTO");
			input.setPsiControl("USE_PSI");
			input.setFilterStrength(0);
			input.setDeblockFilter("DISABLED");
			input.setDenoiseFilter("DISABLED");
			input.setTimecodeSource("ZEROBASED");
			input.setFileInput(fileInput);

			List<Input> inputGroups = new ArrayList<Input>();
			inputGroups.add(input);

			settings.setOutputGroups(outputGroups);
			settings.setAdAvailOffset(0);
			settings.setInputs(inputGroups);

			AccelerationSettings accelerationSettings = new AccelerationSettings();
			accelerationSettings.setMode("DISABLED");

			createJobRequest.withSettings(settings).withAccelerationSettings(accelerationSettings)
					.withStatusUpdateInterval("SECONDS_60").withPriority(0);

			// Create Job request
			CreateJobResult createJobResult = emf.createJob(createJobRequest);
			System.out.println(createJobResult);

			jobID = createJobResult.getJob().getId();

		} catch (AmazonServiceException ase) {
			System.out.println("Caught an AmazonServiceException.");
			System.out.println("Error Message:    " + ase.getMessage());
			System.out.println("HTTP Status Code: " + ase.getStatusCode());
			System.out.println("AWS Error Code:   " + ase.getErrorCode());
			System.out.println("Error Type:       " + ase.getErrorType());
			System.out.println("Error Description:" + ase.getCause());
			System.out.println("Request ID:       " + ase.getRequestId());
		} catch (AmazonClientException ace) {
			System.out.println("Caught an AmazonClientException, which means the client encountered "
					+ "a serious internal problem while trying to communicate with the services, "
					+ "such as not being able to access the network.");
			System.out.println("Error Message: " + ace.getMessage());
		}
		return jobID;
	}

	public AWSMediaConvert getMediaConvert() {
	
		AWSMediaConvert emf = AWSMediaConvertClientBuilder.standard().withRegion(region).build();
		EndpointConfiguration endpointConfiguration = new EndpointConfiguration(endpoint, region);
		emf = AWSMediaConvertClientBuilder.standard().withEndpointConfiguration(endpointConfiguration).build();

		return emf;
	}
}