public class SplitVideo {

	public static void main(final String[] args) {

		JobConfig jobConfig = new JobConfig();
		JobMonitor jobMonitor = new JobMonitor();
		String[] jobs = new String[4];
		
		//mock up url
		String fileInput = "s3://sbx-test-fr-videos/46080722/0c994ca0-56f2-4d82-a9f5-abaa8d060a42/archive.mp4";
		
		//mock up url, to be replaced if we have another way to set the output in the same directory as the input
		String fileOutput = fileInput.substring(0, fileInput.lastIndexOf("/")+1);
		String status;

		System.out.println("INPUT: " + fileInput);
		System.out.println("OUTPUT: " + fileOutput);
		
		jobs[0] = jobConfig.configure("00:00:00;00", "00:30:05;00", "_1",
				"arn:aws:mediaconvert:eu-central-1:766219056525:queues/Queue_slice_1", fileInput, fileOutput);
		jobs[1] = jobConfig.configure("00:30:00;00", "01:00:05;00", "_2",
				"arn:aws:mediaconvert:eu-central-1:766219056525:queues/Queue_slice_2", fileInput, fileOutput);
		jobs[2] = jobConfig.configure("01:00:00;00", "01:30:05;00", "_3",
				"arn:aws:mediaconvert:eu-central-1:766219056525:queues/Queue_slice_3", fileInput, fileOutput);
		jobs[3] = jobConfig.configure("01:30:00;00", "02:05:00;00", "_4",
				"arn:aws:mediaconvert:eu-central-1:766219056525:queues/Queue_slice_4", fileInput, fileOutput);
		for (String job : jobs) {
			status = jobMonitor.checkFinalStatus(job, jobConfig.getMediaConvert());
			System.out.println("JOB "+ job + " "+ status);
		}
	}
}
