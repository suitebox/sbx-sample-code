package com.poc.intelliflo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IntellifloApplication {

	public static void main(String[] args) {
		SpringApplication.run(IntellifloApplication.class, args);
	}

}
