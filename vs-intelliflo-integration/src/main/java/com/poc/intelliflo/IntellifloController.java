package com.poc.intelliflo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Redirect;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/v1")
public class IntellifloController {

	@Value("${auth.clientID}")
	private String clientID;

	@Value("${auth.secret}")
	private String clientSecret;
	
	private final HttpClient httpClient = HttpClient
											.newBuilder()
//											.followRedirects(Redirect.NORMAL)
											.version(HttpClient.Version.HTTP_2)
											.build();

	private ObjectMapper mapper = new ObjectMapper();


	@GetMapping("/redirect")
	public void redirect() throws IOException, InterruptedException {
//		String baseUrl = "https://identity.intelliflo.com/core/connect/authorize?";
//		String param = "response_type=code"
//				+ "&scope=openid+myprofile+profile+client_data"
//				+ "&client_id=app-611fb9b-acf-fe1edccbfa5c4aefb047a74975c6fe95"
//				+ "&redirect_uri=http://localhost:9081/intelliflo/v1/auth";
//		
//		 HttpRequest request = HttpRequest.newBuilder()
//	                .GET()
//	                .uri(URI.create(baseUrl+""+param))
//	                .build();
//
//	        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
//	        System.out.println(response.statusCode());
//	        System.out.println(response.uri().toString());
//	        System.out.println(response.body());
//	        
	}

	@GetMapping("/auth")
	public void auth(
			@RequestParam(value = "code", required = false) String code,
			@RequestParam(value = "client_id", required = false) String client_id

	) throws IOException, URISyntaxException, InterruptedException {

		if (code != null) {

			Map<Object, Object> data = new HashMap<>();
			data.put("grant_type", "authorization_code");
			data.put("code", code);
			data.put("redirect_uri", "http://localhost:9081/intelliflo/v1/auth");
			
			String authCode = clientID+":"+clientSecret;
			String encodedAuthCode = Base64.getEncoder().encodeToString(authCode.getBytes());
			HttpRequest request = HttpRequest.newBuilder().POST(buildFormDataFromMap(data))
					.uri(URI.create("https://identity.intelliflo.com/core/connect/token"))
					.setHeader("Content-Type", "application/x-www-form-urlencoded")
					.setHeader("Authorization",
							"Basic " + encodedAuthCode)
					.build();

			HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

			System.out.println(response.statusCode());
			if (response.statusCode() == 200) {
				String json = response.body();
				TokenDto tokenDto = mapper.readValue(json, TokenDto.class);

				System.out.println("======> Access Token: " + tokenDto.getAccess_token());
			} else {
				System.out.println("======> Status code: " + response.statusCode());
			}
		}

		if (client_id != null) {
			System.out.println("======> client_id: " + client_id);
		}

//		https://identity.intelliflo.com/core/connect/authorize?response_type=code&scope=openid myprofile profile client_data&client_id=app-611fb9b-acf-fe1edccbfa5c4aefb047a74975c6fe95&redirect_uri=http://localhost:9081/intelliflo/v1/auth

	}


	private static HttpRequest.BodyPublisher buildFormDataFromMap(Map<Object, Object> data) {
		var builder = new StringBuilder();
		for (Map.Entry<Object, Object> entry : data.entrySet()) {
			if (builder.length() > 0) {
				builder.append("&");
			}
			builder.append(URLEncoder.encode(entry.getKey().toString(), StandardCharsets.UTF_8));
			builder.append("=");
			builder.append(URLEncoder.encode(entry.getValue().toString(), StandardCharsets.UTF_8));
		}
		System.out.println(builder.toString());
		return HttpRequest.BodyPublishers.ofString(builder.toString());
	}



}
